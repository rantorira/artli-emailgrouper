#!perl

#****************************************************************************#
#************************* Functional testing *******************************#
#****************************************************************************#

use 5.18.0;
use strict;

use File::Temp;
use Test::Spec;
use Test::Trap;

use ArtLi::EmailGrouper;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'ArtLi::EmailGrouper';

binmode( STDOUT, ':utf8' );

#=============================================================================
############################# Object tests ###################################

describe "EmailGrouper," => sub {

    #=========================================================================
    # Tests config

    my $mock_file;

    my $in = <<'TXT';
email@xtest.ru
email@xtest.ru
email@ztest.ru
email@ztest.ru
invalid@
email@test.ru
email@test.ru
@invalid
email@test.ru
email@ctest.ru
email@atest.ru
email@atest.ru
invalid@email ru
invalid@email\.ru
email@test.ru
email@test.ru
email@atest.ru
TXT

    my $out_normal = <<'TXT';
atest.ru 3
ctest.ru 1
test.ru 5
xtest.ru 2
ztest.ru 2
INVALID 4
TOTAL 17
TXT

    my $out_nostd2003 = <<'TXT';
atest.ru 3
ctest.ru 1
email ru 1
email\.ru 1
test.ru 5
xtest.ru 2
ztest.ru 2
INVALID 2
TOTAL 17
TXT

    my $fixtures = {
        in              => $in,
        out_normal      => $out_normal,
        out_nostd2003   => $out_nostd2003,
    };

    # Instance constructor, Dependencies STUBS and MOCKS etc.
    before each => sub {

        # External dependency
        $mock_file = File::Temp->new( UNLINK => 0 );
        $mock_file->autoflush( 1 );
    };

    # Destructors
    after each => sub {

        unlink $mock_file->filename if -e $mock_file->filename;
    };

    after all => sub {

        # If `it` section was failed
        unlink $mock_file->filename if -e $mock_file->filename;
    };

    #=========================================================================
    # Test cases

    it "should work" => sub {
        print $mock_file $fixtures->{ in };

        local @ARGV = ( '-f', $mock_file->filename );
        my $class_instance = $module_name->new_with_options();

        # Act
        trap{ $class_instance->run() };

        # Assert
        like( $trap->stdout, qr/\Q$fixtures->{ out_normal }\E/ );
    };


    it "should work with --nostd2003 flag" => sub {
        print $mock_file $fixtures->{ in };

        local @ARGV = ( '-f', $mock_file->filename, '--nostd2003' );
        my $class_instance = $module_name->new_with_options();

        # Act
        trap{ $class_instance->run() };

        # Assert
        like( $trap->stdout, qr/\Q$fixtures->{ out_nostd2003 }\E/ );
    };
};

#=============================================================================
############################### Run tests  ###################################

runtests;

