#!perl

use 5.18.0;
use strict;

use File::Temp;
use Test::Exception;
use Test::Spec;
use Test::Trap;

use ArtLi::EmailGrouper;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'ArtLi::EmailGrouper';

#=============================================================================
############################### Unit tests ###################################

describe "Method '_process_file'" => sub {

    #=========================================================================
    # Tests config

    my $class_instance;
    my $mock_file;

    my $fixtures = {
        empty_stub              => '',
        nonexistent_file_name   => 'nonexistent_file_.txt',
        normal_string           => 'string',
        whitespace_string       => '  string 123  ',
        trimmed_string          => 'string 123',
        empty_lines             => "     \n \n\n\n   \n  \n",
        some_critical_error     => 'some text',
    };

    # Instance constructor, Dependencies STUBS and MOCKS etc.
    before each => sub {

        # External dependency
        $mock_file = File::Temp->new( UNLINK => 0 );
        $mock_file->autoflush( 1 );

        # Constructor
        local @ARGV = ( '-f', 'fake' );
        $class_instance = $module_name->new_with_options();

        # Internal dependency
        $class_instance->stubs( '_grouping' => $fixtures->{ empty_stub } );
    };

    # Destructors
    after each => sub {

        unlink $mock_file->filename if -e $mock_file->filename;
    };

    after all => sub {

        # If `it` section was failed
        unlink $mock_file->filename if -e $mock_file->filename;
    };

    #=========================================================================
    # Test cases

    describe "when executing," => sub {

        it "should die if file for work cannot be opened" => sub {

            $class_instance->_set_source_file( 
                $fixtures->{ nonexistent_file_name } 
            );

            # Act & Assert
            Test::Exception::dies_ok { $class_instance->_process_file() };
        };
    };

    #-------------------------------------------------------------------------

    describe "when read from file," => sub {

        my $expect_method = '_grouping';

        it "should invoke '$expect_method' method for line processing" => sub {

            my $invokation_times = 3;

            $class_instance->_set_source_file( $mock_file->filename );
            say $mock_file $fixtures->{ normal_string } for 1 .. $invokation_times;

            # Internal dependency
            $class_instance->expects( $expect_method )
                ->exactly( $invokation_times );

            # Act
            $class_instance->_process_file();

            ok 1;
        };


        it "should pass into '$expect_method' method 2 attributes - first, is `readed lines`" => sub {

            $class_instance->_set_source_file( $mock_file->filename );
            print $mock_file $fixtures->{ normal_string };

            # Spy
            my $control_point;

            # Internal dependency
            $class_instance->stubs( $expect_method =>  sub {
                        my $test_spec = shift;
                        my ( $first_attr, $second_attr ) = @_;

                        $control_point = $first_attr;
                    }
                );

            # Act
            $class_instance->_process_file();

            is( $control_point, $fixtures->{ normal_string } );
        };


        it "should pass into '$expect_method' method 2 attributes - second, is `HASH REF`" => sub {

            $class_instance->_set_source_file( $mock_file->filename );
            print $mock_file $fixtures->{ normal_string };

            # Spy
            my $control_point;

            # Internal dependency
            $class_instance->stubs( $expect_method =>  sub {
                    my $test_spec = shift;
                    my ( $first_attr, $second_attr ) = @_;

                    $control_point = ref $second_attr;
                }
            );

            # Act
            $class_instance->_process_file();

            is( $control_point, "HASH" );
        };


        it "should intercept errors if '$expect_method' method failed" => sub {

            $class_instance->_set_source_file( $mock_file->filename );
            print $mock_file $fixtures->{ normal_string };

            # Internal dependency
            $class_instance->expects( $expect_method )
                ->returns( sub { die $fixtures->{ some_critical_error } } );

            # Act
            trap{ $class_instance->_process_file() };

            like( $trap->stderr, qr/\Q$fixtures->{ some_critical_error }\E/);
        };


        it "should trim processed lines" => sub {

            $class_instance->_set_source_file( $mock_file->filename );
            print $mock_file $fixtures->{ whitespace_string };

            # Spy
            my $control_point;

            # Internal dependency
            $class_instance->stubs( $expect_method =>  sub {
                    my $test_spec = shift;
                    my ( $first_attr, $second_attr ) = @_;

                    $control_point = $first_attr;
                }
            );

            # Act
            $class_instance->_process_file();

            is( $control_point, $fixtures->{ trimmed_string } );
        };


        it "should miss empty lines" => sub {

            $class_instance->_set_source_file( $mock_file->filename );
            print $mock_file $fixtures->{ empty_lines };

            # Internal dependency
            $class_instance->expects( $expect_method )
                ->never;

            # Act
            $class_instance->_process_file();

            ok 1;
        };
    };
};

#=============================================================================
############################### Run tests  ###################################

runtests;

