#!perl

use 5.18.0;
use strict;

use File::Temp;
use Test::Exception;
use Test::Spec;
use Test::Trap;

use ArtLi::EmailGrouper;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'ArtLi::EmailGrouper';

#=============================================================================
############################### Unit tests ###################################

describe "Method 'run'" => sub {

    #=========================================================================
    # Tests config

    my $class_instance;
    my $mock_file;

    my $sort_result = <<'TXT';
a 3
b 2
c 1
TXT

    my $fixtures = {
        nonexistent_file_name   => 'nonexistent_file_.txt',
        any_string              => 'string',
        grouped_hash            => { c => 1, a => 3, b => 2 },
        sort_result             => $sort_result,
        invalid_lines_count     => 20,
        grouped_lines_count     => 123,
    };

    # Instance constructor, Dependencies STUBS and MOCKS etc.
    before each => sub {

        # External dependency
        $mock_file = File::Temp->new( UNLINK => 0 );
        $mock_file->autoflush( 1 );

        # constructor
        local @ARGV = ( '-f', 'fake' );
        $class_instance = $module_name->new_with_options();

        # Internal dependency
        $class_instance->stubs( '_process_file' => {} );
    };

    # Destructors
    after each => sub {

        unlink $mock_file->filename if -e $mock_file->filename;
    };

    after all => sub {

        # If `it` section was failed
        unlink $mock_file->filename if -e $mock_file->filename;
    };

    #=========================================================================
    # Test cases

    describe "when executing," => sub {

        it "should die if file for work does not exists" => sub {
            $class_instance->_set_source_file(
                $fixtures->{ nonexistent_file_name }
            );

            # Act & Assert
            Test::Exception::dies_ok { $class_instance->run() };
        };


        it "should die if file for work is empty" => sub {

            # File is empty
            $class_instance->_set_source_file( $mock_file->filename );

            # Act & Assert
            Test::Exception::dies_ok { $class_instance->run() };
        };


        it "should invoke '_process_file' method" => sub {

            # File exists
            $class_instance->_set_source_file( $mock_file->filename );

            # File no empty
            print $mock_file $fixtures->{ any_string };

            # Internal dependency
            $class_instance->expects( '_process_file' )
                ->at_least_once;

            # Act
            trap{ $class_instance->run() };

            is( $trap->exit, 0 );
        };
    };

    #-------------------------------------------------------------------------

    describe "when done," => sub {

        it "should terminate with Unix-like exit code 0 if was not broken" => sub {

            # File exists
            $class_instance->_set_source_file( $mock_file->filename );

            # File is no empty
            print $mock_file $fixtures->{ any_string };

            # Internal dependency
            $class_instance->stubs( '_process_file' => {} );

            # Act
            trap{ $class_instance->run() };

            is( $trap->exit, 0 );
        };


        it "should sort and print '_process_file' result" => sub {

            # File exists
            $class_instance->_set_source_file( $mock_file->filename );

            # File is no empty
            print $mock_file $fixtures->{ any_string };

            # Internal dependency
            $class_instance->expects( '_process_file' )
                ->returns( $fixtures->{ grouped_hash } );

            # Act
            trap{ $class_instance->run() };

            is( $trap->stdout, $fixtures->{ sort_result } );
        };


        it "should print '_invalid_lines_counter' attribure value if there is" => sub {

            # File exists
            $class_instance->_set_source_file( $mock_file->filename );

            # File is no empty
            print $mock_file $fixtures->{ any_string };

            # Internal dependency
            $class_instance->stubs( '_process_file' => {} );
            $class_instance->stubs(
                'get_invalid_lines_count' => $fixtures->{ invalid_lines_count }
            );

            # Act
            trap{ $class_instance->run() };

            like(
                $trap->stdout,
                qr/INVALID\s\Q$fixtures->{ invalid_lines_count }\E/
            );
        };


        it "should print '_grouped_lines_counter' attribure value if there is" => sub {

            # File exists
            $class_instance->_set_source_file( $mock_file->filename );

            # File is no empty
            print $mock_file $fixtures->{ any_string };

            # Internal dependency
            $class_instance->stubs( '_process_file' => {} );
            $class_instance->stubs(
                'get_grouped_lines_count' => $fixtures->{ grouped_lines_count }
            );

            # Act
            trap{ $class_instance->run() };

            like(
                $trap->stdout,
                qr/TOTAL\s\Q$fixtures->{ grouped_lines_count }\E/
            );
        };
    };
};

#=============================================================================
############################### Run tests  ###################################

runtests;

