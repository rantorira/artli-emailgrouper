#!perl

use 5.18.0;
use strict;
use warnings FATAL => 'all';

use Test::More tests => 1;

#-----------------------------------------------------------------------------

subtest 'ArtLi::EmailGrouper can be loaded' => sub {
    plan tests => 1;

    require_ok( 'ArtLi::EmailGrouper' );
    note( 
        "Testing ArtLi::EmailGrouper $ArtLi::EmailGrouper::VERSION,"
      . "Perl $^V, $^X" 
    );
};

