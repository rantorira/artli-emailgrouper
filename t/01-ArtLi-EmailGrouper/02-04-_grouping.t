#!perl

use 5.18.0;
use strict;

use Test::Spec;

use ArtLi::EmailGrouper;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'ArtLi::EmailGrouper';

#=============================================================================
############################### Unit tests ###################################

describe "Method '_grouping'" => sub {

    #=========================================================================
    # Tests config

    my $class_instance;
    my $email_validator_stub;
    my $domain_validator_stub;
    my $mock_file;

    my $fixtures = {
        empty_stub              => '',
        any_string              => 'string',
        valid_email             => 'valid@email.test',
        domain_of_valid_email   => 'email.test',
        some_error_text         => 'some_text',
        true                    => 1,
    };

    # Instance constructor, Dependencies STUBS and MOCKS etc.
    before each => sub {

        # External dependency
        $domain_validator_stub = stub();
        $domain_validator_stub->stubs( 'get_validation_error' );
        $domain_validator_stub->stubs( 'has_validation_error' );

        # External dependency
        $email_validator_stub = stub();
        $email_validator_stub->stubs(
            domain_validator => $domain_validator_stub
        );
        $email_validator_stub->stubs( 'get_validation_error' );
        $email_validator_stub->stubs( 'get_domain_part' );
        $email_validator_stub->stubs( 'is_valid' );

        # Constructor
        local @ARGV = ( '-f', 'fake' );
        $class_instance = $module_name->new_with_options();

        # Internal dependency
        $class_instance->stubs( '_email_validator' => $email_validator_stub );
    };

    #=========================================================================
    # Test cases

    describe "when executing," => sub {

        it "should count self invokation" => sub {

            my $invokation_times = 3;

            # Act
            $class_instance->_grouping() for 1 .. $invokation_times;

            is( $class_instance->get_grouped_lines_count, $invokation_times );
        };


        it "shoild group and count if given line is valid email" => sub {

            # Will contain execution result
            my $result_hash = {};

            my $invokation_times = 5;

            # External dependency
            $email_validator_stub->expects( 'is_valid' )
                ->returns( $fixtures->{ true } )
                ->exactly( $invokation_times );

            $email_validator_stub->expects( 'get_domain_part' )
                ->returns( $fixtures->{ domain_of_valid_email } )
                ->exactly( $invokation_times );

            # Act
            $class_instance->_grouping(
                    $fixtures->{ empty_stub }, $result_hash
            ) for 1 .. $invokation_times;

            is(
                $result_hash->{ $fixtures->{ domain_of_valid_email } },
                $invokation_times
            );
        };


        describe "if given line is INvalid email," => sub {

            it "shoild count it" => sub {

                my $invokation_times = 5;

                # External dependency
                $email_validator_stub->expects( 'is_valid' )
                    ->returns( undef )
                    ->exactly( $invokation_times );

                # Act
                $class_instance->_grouping() for 1 .. $invokation_times;

                is( $class_instance->get_invalid_lines_count, $invokation_times );
            };


            describe "if verbose mode is set," => sub {

                it "should print error message" => sub {

                    # Assume verbose mode is set
                    $class_instance->_set_verbose( 1 );

                    # External dependency
                    $email_validator_stub->expects( 'is_valid' )
                        ->returns( undef );

                    $email_validator_stub->expects( 'get_validation_error' )
                        ->returns( $fixtures->{ some_error_text } );

                    # Act
                    trap{ $class_instance->_grouping() };

                    like( $trap->stdout, qr/\Q$fixtures->{ some_error_text }\E/ );
                };
            };
        };
    };
};

#=============================================================================
############################### Run tests  ###################################

runtests;

