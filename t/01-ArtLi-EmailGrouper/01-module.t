#!perl

use 5.18.0;
use strict;

use Test::Differences qw( eq_or_diff );
use Test::Moose qw( has_attribute_ok does_ok );
use Test::More;

use ArtLi::EmailGrouper;

#-----------------------------------------------------------------------------

use utf8;

#=============================================================================
############################### Config #######################################

my $module_name = 'ArtLi::EmailGrouper';

#=============================================================================
############################ Class tests #####################################

#-----------------------------------------------------------------------------
# Class inheritance

subtest "class should have inheritance" => sub {
    plan tests => 1;

    does_ok(
        $module_name, 'MooseX::Getopt', "class expanded via 'MooseX::Getopt'"
    );
};

#-----------------------------------------------------------------------------
# Class attributes

subtest "class should have inherited attributes" => sub {
    plan tests => 1;

    has_attribute_ok(
        $module_name , 'help_flag', "class have attribute 'help_flag'"
    );
};

subtest "class should have attributes" => sub {
    plan tests => 6;

    my @attrs = ( '_email_validator', 'source_file', 'NoUseSTD3ASCIIRules',
        'verbose', '_grouped_lines_counter', '_invalid_lines_counter' );

    foreach my $attr ( @attrs ) {
        has_attribute_ok(
            $module_name , $attr, "class has attribute '$attr'"
        );
    }
};

#-----------------------------------------------------------------------------
# Class methods

subtest "class should have methods" => sub {
    plan tests => 9;

    can_ok( $module_name, 'run' );
    can_ok( $module_name, '_process_file' );
    can_ok( $module_name, '_grouping' );

    subtest "class should have inherited methods" => sub {
        plan tests => 3;

        can_ok( $module_name, 'new_with_options' );
        can_ok( $module_name, 'usage' );
        can_ok( $module_name, 'ARGV' );
    };

    subtest "should have accessors for '_email_validator'" => sub {
        plan tests => 2;

        can_ok( $module_name, '_email_validator' );
        can_ok( $module_name, '_build_email_validator' );
    };

    subtest "should have accessors for 'source_file'" => sub {
        plan tests => 2;

        can_ok( $module_name, '_set_source_file' );
        can_ok( $module_name, 'get_source_file' );
    };

    subtest "should have accessors for 'verbose'" => sub {
        plan tests => 2;

        can_ok( $module_name, '_set_verbose' );
        can_ok( $module_name, 'is_verbose' );
    };

    subtest "should have accessors for '_grouped_lines_counter'" => sub {
        plan tests => 2;

        can_ok( $module_name, 'get_grouped_lines_count' );
        can_ok( $module_name, 'inc_grouped_lines' );
    };

    subtest "should have accessors for '_invalid_lines_counter'" => sub {
        plan tests => 2;

        can_ok( $module_name, 'get_invalid_lines_count' );
        can_ok( $module_name, 'inc_invalid_lines' );
    };

};

#=============================================================================
################################ Run tests  ##################################

done_testing();

