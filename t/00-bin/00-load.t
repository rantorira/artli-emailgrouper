#!perl

use 5.18.0;
use strict;
use warnings FATAL => 'all';

use File::Spec;
use Test::More tests => 1;

#-----------------------------------------------------------------------------
# CLI: artli-emailgrouper

subtest 'artli-emailgrouper can be loaded' => sub {
    plan tests => 1;

    my $CLI = File::Spec->catfile( qw( bin artli-emailgrouper ) );

    local @ARGV = ( '-f', 'fake' );

    require_ok( $CLI );
    note( "Testing CLI: artli-emailgrouper, Perl $^V, $^X" );
};
