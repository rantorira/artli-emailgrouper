#
# Documentation is at the __END__
#

package ArtLi::EmailGrouper {

    use Modern::Perl '2014';
    use Carp;
    use Moose;
    use Try::Tiny;

    use Net::IDN::EmailValidator;
    use Net::IDN::EmailValidator::DomainValidator;

    #-------------------------------------------------------------------------

    use namespace::autoclean;
    use utf8;

    #=========================================================================
    ## Constants

    our $VERSION = '0.01';

    #=========================================================================
    ## Config

    binmode( STDOUT, ':encoding(UTF-8)' );
    with 'MooseX::Getopt';

    #=========================================================================
    ## Attributes

    # Will provides email validator
    has '_email_validator' => (
        is              => 'ro',
        isa             => 'Net::IDN::EmailValidator',
        documentation   => 'Net::IDN::EmailValidator inctance.',
        init_arg        => undef,
        lazy            => 1,
        builder         => '_build_email_validator',
    );

    #-------------------------------------------------------------------------
    # CLI flags

    # Specify source sile
    has 'source_file'   => (
        traits          => [ 'Getopt' ],
        is              => 'ro',
        isa             => 'Str',
        documentation   => 'Mandatory. Specify source file.',
        cmd_flag        => 'file',
        cmd_aliases     => [qw/ f /],
        writer          => '_set_source_file',
        reader          => 'get_source_file',
        required        => 1,
    );

    # Specify IDNA 2003 standart noncompliance
    has 'NoUseSTD3ASCIIRules' => (
        traits          => [ 'Getopt' ],
        is              => 'ro',
        isa             => 'Bool',
        documentation   => 'Flag. If true, IDNA 2003 standart will'
                         . ' noncompliance. For more information look'
                         . ' http://unicode.org/reports/tr46/#IDNA2003-Section',
        cmd_flag        => 'nostd2003',
        writer          => '_set_nostd2003',
        reader          => 'is_nostd2003',
        default         => 0,
    );

    # Specify verbose mode
    has 'verbose' => (
        traits          => [ 'Getopt' ],
        is              => 'ro',
        isa             => 'Bool',
        documentation   => "Flag. If true, validation errors will be shown.",
        cmd_flag        => 'verbose',
        cmd_aliases     => [qw/ v /],
        writer          => '_set_verbose',
        reader          => 'is_verbose',
        default         => 0,
    );

    # Will count grouped lines
    has '_grouped_lines_counter' => (
        traits          => [ 'Counter' ],
        is              => 'ro',
        isa             => 'Int',
        documentation   => 'Grouped lines counter',
        reader          => 'get_grouped_lines_count',
        handles         => { inc_grouped_lines => 'inc' },
        default         => 0,
        init_arg        => undef,
    );

    # Will count grouped lines
    has '_invalid_lines_counter' => (
        traits          => [ 'Counter' ],
        is              => 'ro',
        isa             => 'Int',
        documentation   => 'Invalid lines counter',
        reader          => 'get_invalid_lines_count',
        handles         => { inc_invalid_lines => 'inc' },
        default         => 0,
        init_arg        => undef,
    );

    #=========================================================================
    ## Methods

    sub run {
        my ( $self ) = @_;

        croak "Bad file : " . $self->get_source_file . "\n"
          unless -e -f $self->get_source_file;

        croak "File : " . $self->get_source_file . " - is empty \n"
          unless -s $self->get_source_file;

        # Process
        my $result_hash_ref = $self->_process_file;

        # Output
        say "$_ $result_hash_ref->{ $_ }" for sort keys %$result_hash_ref;

        say "INVALID " . $self->get_invalid_lines_count
          if $self->get_invalid_lines_count;

        say "TOTAL " . $self->get_grouped_lines_count
          if $self->get_grouped_lines_count;

        exit 0;
    }

    #=========================================================================
    ## PRIVATE methods

    sub _process_file {
        my ( $self ) = @_;

        open my $fh, "<:encoding(utf-8)", $self->get_source_file
          or croak "Can't open file ' . $self->get_source_file . ' for reading: $!";

        # Will contain result
        my $result_hash_ref = {};

        while ( my $line = <$fh> ) {
            chomp $line;
            $line =~ s/^\s+//;
            $line =~ s/\s+$//;

            next unless $line;

            try {
                $self->_grouping( $line, $result_hash_ref );
            }
            catch {
                chomp;
                carp "Error: $_";
            };
        }

        close $fh;
        return $result_hash_ref;
    }

    sub _grouping {
        # $line: string from file
        my ( $self, $line, $result_hash_ref ) = @_;

        $self->inc_grouped_lines();

        if ( $self->_email_validator->is_valid( email => $line ) ) {
            $result_hash_ref->{ $self->_email_validator->get_domain_part }++;
        }
        else {
            $self->inc_invalid_lines();

            say $self->_email_validator->get_validation_error
              if $self->is_verbose;
        }
    }

    sub _build_email_validator {
        my $self = shift;

        return Net::IDN::EmailValidator->new(
            domain_validator => Net::IDN::EmailValidator::DomainValidator->new(
                UseSTD3ASCIIRules => $self->is_nostd2003 ? 0 : 1
            )
        );
    }

    __PACKAGE__->meta->make_immutable();

};

#-----------------------------------------------------------------------------

1;

__END__

=pod

=head1 NAME

ArtLi::EmailGrouper - Souce code for C<artli-emailgrouper> tool.

=head1 VERSION

Version 0.01

=head1 SYNOPSIS

    use ArtLi::EmailGrouper;


    my $ceg = ArtLi::EmailGrouper->new_with_options();
    $ceg->run();


=head1 DESCRIPTION

Provides simple interface for grouping and counting equivalent emails from
given file.

=head1 SUBROUTINES/METHODS

=over 4

=back

=head2 run()

This method run CLI processing

=head1 AUTHOR

Art Li, <job.lit.art at gmail.com>


=cut
