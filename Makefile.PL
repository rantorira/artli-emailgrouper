use 5.18.0;
use strict;
use warnings FATAL => 'all';
use ExtUtils::MakeMaker;

WriteMakefile(
    NAME                            => 'ArtLi::EmailGrouper',
    AUTHOR                          => q{Art Li <job.lit.art@gmail.com>},
    VERSION_FROM                    => 'lib/ArtLi/EmailGrouper.pm',
    ABSTRACT_FROM                   => 'lib/ArtLi/EmailGrouper.pm',
    LICENSE                         => 'Artistic_2_0',
    EXE_FILES                       => [ 'bin/artli-emailgrouper' ],
    MIN_PERL_VERSION                => 5.18.0,

    CONFIGURE_REQUIRES => {
        'ExtUtils::MakeMaker'       => 0,
    },

    TEST_REQUIRES => {
        'File::Spec'                => 0,
        'File::Temp'                => 0,
        'Test::Differences'         => 0,
        'Test::More'                => 0,
        'Test::Moose'               => 0,
        'Test::Exception'           => 0,
        'Test::Trap'                => 0,
        'Test::Spec'                => 0,
    },

    PREREQ_PM => {
        'Modern::Perl'              => 1.20140107,
        'MooseX::Getopt'            => 0.63,
        'Net::IDN::EmailValidator'  => 0,
        'Try::Tiny'                 => 0,

    },

    dist    => { COMPRESS => 'gzip -9f', SUFFIX => 'gz', },
    clean   => { FILES => 'ArtLi-EmailGrouper-*' },
    test    => { RECURSIVE_TEST_FILES => 1 },
);
